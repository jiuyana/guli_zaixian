package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author lsx
 * @since 2020-08-01
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {

}
