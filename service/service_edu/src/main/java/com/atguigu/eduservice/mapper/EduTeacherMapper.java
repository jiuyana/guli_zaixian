package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author lsx
 * @since 2020-07-22
 */
@Mapper
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
