package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.PrintStream;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author lsx
 * @since 2020-08-01
 */
@RestController
@RequestMapping("/eduservice/course")
@CrossOrigin
public class EduCourseController {

    @Autowired
    private EduCourseService eduCourseService;

    @PostMapping("/addCouserInfo")
    public  R addCouserInfo(@RequestBody CourseInfoVo courseInfoVo){

        eduCourseService.saveCourseInfo(courseInfoVo);
        return R.ok();
    }
}

