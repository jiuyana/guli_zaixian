package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.vo.TeacherQuery;
import com.atguigu.eduservice.service.EduTeacherService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author lsx
 * @since 2020-07-22
 */
@Api(description="讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin
public class EduTeacherController {

    @Resource
    private EduTeacherService eduTeacherService;

    /**
     * 查询讲师所有数据
     * @return 返回json格式数据
     */
    @ApiOperation(value = "所有讲师列表")
    @GetMapping("/findAll")//这里'/'加不加都对
    public R findAllTeacher(){
        List<EduTeacher> list = eduTeacherService.list(null);
        return R.ok().data("items",list);
    }
    /**
     * 逻辑删除讲师的方法
     */
    @ApiOperation(value = "根据id删除讲师")
    @DeleteMapping("{id}")
    public  R removeTeacher(@ApiParam(name = "id", value = "讲师ID", required = true)@PathVariable String id){
        boolean flag = eduTeacherService.removeById(id);
        if(flag){
            return R.ok();
        }else
            return R.error();
    }
    //分页接口
    @ApiOperation(value = "讲师分页")
    @GetMapping("/pageTeacher/{current}/{limit}")
    public  R pageTeacherList(@PathVariable Long current , @PathVariable Long limit){
        //创建分页对象
        Page<EduTeacher> pageTeacher = new Page<>(current,limit);
        //调用方法实现分页  底层封装 将分页的所有的数据封装到pageTeacher中
        eduTeacherService.page(pageTeacher,null);

        /* try {
            int i = 10/0;
        } catch (Exception e) {
            throw  new GuliException(2000,"执行了自定义异常类");
        }*/

        long total = pageTeacher.getTotal();//获取总记录数

        List<EduTeacher> records = pageTeacher.getRecords();//获取数据的list集合


        return R.ok().data("total",total).data("rows",records);
    }
    //多条件组合查询带分页
    @ApiOperation(value = "多条件组合查询带分页")
    @PostMapping("/pageTeacherCondition/{current}/{limit}")
    public R pageTeacherCondition(@PathVariable Long current ,
                                  @PathVariable Long limit,
                                  @RequestBody(required = false) TeacherQuery teacherQuery){
                                    //使用RequestBody注解 将前端请求的条件封装在teacherQuery对象中
      Page<EduTeacher> teacherPage = new Page<>(current,limit);

        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        //写条件查询
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();
        if(!StringUtils.isEmpty(name)){
            wrapper.like("name",name);
        }
        if(!StringUtils.isEmpty(level)){
            wrapper.like("level",level);
        }
        if(!StringUtils.isEmpty(begin)){
            wrapper.like("gmt_create",begin);
        }
        if(!StringUtils.isEmpty(end)){
            wrapper.like("gmt_create",end);
        }
        wrapper.orderByDesc("gmt_create");
        eduTeacherService.page(teacherPage,wrapper);

        long total = teacherPage.getTotal();
        List<EduTeacher> records = teacherPage.getRecords();
        return R.ok().data("total",total).data("rows",records);

    }
        //添加讲师的接口
    @ApiOperation(value = "添加讲师")
    @PostMapping("/addTeacher")
    public  R  addTeacher(@RequestBody EduTeacher eduTeacher){
        //@RequestBody EduTeacher eduTeacher 存储前端提交的数据 最后添加到数据库
        boolean save = eduTeacherService.save(eduTeacher);
        if (save){
            return R.ok();
        }else {
            return  R.error();
        }
    }
    //根据id查询讲师
    @ApiOperation(value = "根据id查询讲师")
    @GetMapping("/getTeacher/{id}")
    public  R getTeacher(@PathVariable String id){

        EduTeacher eduTeacher = eduTeacherService.getById(id);

        return  R.ok().data("teacher",eduTeacher);
    }
    //修改讲师
    @ApiOperation(value = "修改讲师")
    @PostMapping("/updateTeacher")
    public  R updateTeacher(@RequestBody EduTeacher teacher){
        //修改时需要带上讲师的id
        boolean flag = eduTeacherService.updateById(teacher);
        if (flag){
            return R.ok();
        }else {
            return  R.error();
        }
    }

}

