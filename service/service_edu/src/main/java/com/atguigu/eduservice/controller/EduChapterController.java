package com.atguigu.eduservice.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author lsx
 * @since 2020-08-01
 */
@RestController
@RequestMapping("/eduservice/edu-chapter")
public class EduChapterController {

}

