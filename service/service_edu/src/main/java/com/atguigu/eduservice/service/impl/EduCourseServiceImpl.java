package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduCourseDescription;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.mapper.EduCourseMapper;
import com.atguigu.eduservice.service.EduCourseDescriptionService;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author lsx
 * @since 2020-08-01
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService eduCourseDescriptionService;
    @Override//保存课程信息 将VO中的值分别存到数据库课程表和课程简介表中
    public void saveCourseInfo(CourseInfoVo courseInfoVo) {
        //将vo对象中的数据转换到eduCourse
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVo,eduCourse);
        //将eduCourse中的数据保存到了对应的表中
        eduCourse.setSubjectParentId("");
        int insert = baseMapper.insert(eduCourse);
        if (insert<=0){
            throw  new GuliException(20001,"执行了全局异常");
        }
        String cid = eduCourse.getId();
        //2.将eduCourseDescription中的数据保存到它所对应的表中 注入eduCourseDescriptionService即可
        EduCourseDescription eduCourseDescription = new EduCourseDescription();
        eduCourseDescription.setDescription(courseInfoVo.getDescription());
        //课程表与课程描述表是一对一的关系  将课程id设置到课程描述表中 这样就建立了联系
        eduCourseDescription.setId(cid);
        eduCourseDescriptionService.save(eduCourseDescription);

    }
}
