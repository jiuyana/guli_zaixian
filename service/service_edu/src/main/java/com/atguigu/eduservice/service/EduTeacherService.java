package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author lsx
 * @since 2020-07-22
 */
public interface EduTeacherService extends IService<EduTeacher> {

}
