package com.atguigu.eduservice.entity.subject;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class OneSubject {
    private  String id;
    private  String title;

    //让一级分类与二级分类建立联系
    private List<TwoSubject> children = new ArrayList<>();
}
