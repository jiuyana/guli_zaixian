package com.atguigu.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.ExcelSubjectData;
import com.atguigu.eduservice.entity.subject.OneSubject;
import com.atguigu.eduservice.entity.subject.TwoSubject;
import com.atguigu.eduservice.listener.SubjectExcelListener;
import com.atguigu.eduservice.mapper.EduSubjectMapper;
import com.atguigu.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author lsx
 * @since 2020-07-30
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override
    public void saveSubject(MultipartFile file,EduSubjectService eduSubjectService) {
        try {
            InputStream in = file.getInputStream();
            //读取文件
            EasyExcel.read(in, ExcelSubjectData.class,new SubjectExcelListener(eduSubjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<OneSubject> getAllOneTwoSubject() {
        /*
        * 需求是获取所有的一二级分类
        * */
        //1.获取所有的一级分类  你要查的是数据库  实体类使用数据库对应的实体类
        QueryWrapper<EduSubject> oneWrapper = new QueryWrapper<>();
        oneWrapper.eq("parent_id","0");
        //通常在service下是是调用dao的接口  但是使用了mp之后 查看service类的父类可知 在父类中已经注入了baseMapper 这里直接使用即可
        List<EduSubject> oneSubject = baseMapper.selectList(oneWrapper);

        //2.获取所有的二级分类
        QueryWrapper<EduSubject> twowrapper = new QueryWrapper<>();
        twowrapper.ne("parent_id","0");
        //通常在service下是是调用dao的接口  但是使用了mp之后 查看service类的父类可知 在父类中已经注入了baseMapper 这里直接使用即可
        List<EduSubject> twoSubject = baseMapper.selectList(twowrapper);

        //创建list集合 用于存储最终封装的数据
        List<OneSubject> finalList = new ArrayList<>();
        //3.封装一级分类
        for (EduSubject subjectOne : oneSubject) {
            //将subject中的对香港复制到OneSubject实体类中
            OneSubject oneSubjectData = new OneSubject();
            //正常方法
          /*  oneSubjectData.setId(subjectOne.getId());
            oneSubjectData.setTitle(subjectOne.getTitle());
            可以使用spring包下的BeanUtils工具列
            */
            BeanUtils.copyProperties(subjectOne,oneSubjectData);
            List<TwoSubject> to = new ArrayList<>();
            for (EduSubject subjectTwo : twoSubject) {
                if(subjectOne.getId().equals(subjectTwo.getParentId())){
                    TwoSubject twoSubjectData = new TwoSubject();
                    BeanUtils.copyProperties(subjectTwo,twoSubjectData);

                    to.add(twoSubjectData);
                }


            }
            oneSubjectData.setChildren(to);
            finalList.add(oneSubjectData);
        }
        //4.封装二级分类

        return finalList;
    }
}
