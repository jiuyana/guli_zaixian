package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.subject.OneSubject;
import com.atguigu.eduservice.service.EduSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author lsx
 * @since 2020-07-30
 */
@RestController
@RequestMapping("/eduservice/subject")
@CrossOrigin
public class EduSubjectController {

    @Autowired
    private EduSubjectService eduSubjectService;
    //添加课程分类
    //获取上传过来的文件 把文件读取
    @PostMapping("/addSubject")
    public R addSubject(MultipartFile file){//MultipartFile 该对象用来存储上传来的excel文件
            eduSubjectService.saveSubject(file,eduSubjectService);
        return R.ok();
    }
    //获取课程分类
    @GetMapping("/getAllSubject")
    public  R  getAllSubject(){
        //list结合的泛型是一级分类  原因是一级分类有它本身还有二级
        List<OneSubject> list = eduSubjectService.getAllOneTwoSubject();
        return  R.ok().data("list",list);
    }
}

