package cn.atguigu.excel;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

public class TestEasyexcel {
    public static void main(String[] args) {
        //实现excel写操作

        //定义写入文件的位置
//        String fileName="c:\\write.xlsx";
        //使用easyexcel中的方法实现写操作
        //write方法的参数  第一个参数是 文件路径，第二个是实体类class
//        EasyExcel.write(fileName,DeomData.class).sheet("学生列表").doWrite(getData());
    read();

    }

    /**
     * excel读操作
     */
    public  static  void read(){
        //定义读取文件的路径
        String fileName = "c:\\write.xlsx";

        //读取
        EasyExcel.read(fileName,DeomData.class,new EasyExcelListener()).sheet().doRead();
    }
    //创建方法返回list集合
    public static List<DeomData> getData() {
        List<DeomData> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DeomData data = new DeomData();
            data.setSno(i);
            data.setSname("lsx"+i);
            list.add(data);
        }
        return list;
    }
}
