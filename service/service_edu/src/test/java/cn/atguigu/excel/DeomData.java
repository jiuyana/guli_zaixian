package cn.atguigu.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class DeomData {
    //设置excel表头名称
    @ExcelProperty(value = "学生编号",index = 0)
    private  Integer sno;

    @ExcelProperty(value = "学生名称",index = 1)
    private  String  sname;
}
