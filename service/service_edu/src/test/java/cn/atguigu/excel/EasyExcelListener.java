package cn.atguigu.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;
//创建读取excel的监听器
public class EasyExcelListener extends AnalysisEventListener<DeomData> {

    @Override//读取excel内容
    public void invoke(DeomData deomData, AnalysisContext analysisContext) {
        System.out.println("*****"+deomData);
    }

    @Override//读取excel表头信息
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头"+headMap);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
