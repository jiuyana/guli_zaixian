package com.atguigu.oss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.atguigu.oss.service.OssService;
import com.atguigu.oss.utils.ConstantPropertiesUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@Service
public class OssServiceImpl implements OssService {
    @Override
    public String uploadFileAvatar(MultipartFile file) {
        //实现类中写的是 文件上传的业务逻辑

        String endpoint = ConstantPropertiesUtils.END_POINT;

        String accessKeyId = ConstantPropertiesUtils.ACCESS_KEY_ID;
        String accessKeySecret = ConstantPropertiesUtils.ACCESS_KEY_SECRET;
        String bucket = ConstantPropertiesUtils.BUCKET_NAME;
        try {
            // 创建OSSClient实例。
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 上传文件流。
            InputStream inputStream = file.getInputStream();

           /* 获取文件名称  相同文件名称文件会出现覆盖问题
            String fileName = file.getOriginalFilename();*/
         //使用UUID解决
            String fileName = UUID.randomUUID().toString().replaceAll("-","")+file.getOriginalFilename();

            //将上传的文件根据日期分类管理 可以用new Date()  SimpleDateFormat
            //此处使用joda中的工具类
            String dateTime = new DateTime().toString("yyyy/MM/dd");
            fileName=dateTime+"/"+fileName;


            //调用oss方法putObject实现文件上传
            //参数一 bucket 参数二 上传到oss的文件路径和文件名称  参数三 上传文件输入流
            ossClient.putObject(bucket, fileName, inputStream);

            //https://mygulimall-test.oss-cn-beijing.aliyuncs.com/951kzd.png
            String url ="https://"+bucket+"."+endpoint+"/"+fileName;

            // 关闭OSSClient。
            ossClient.shutdown();
            return url;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }
}
