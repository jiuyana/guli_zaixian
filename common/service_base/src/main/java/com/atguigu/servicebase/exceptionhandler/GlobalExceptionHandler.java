package com.atguigu.servicebase.exceptionhandler;

import com.atguigu.commonutils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    @ResponseBody//因为不是在controller下  返回数据需要加这个注解
    public  R  error(Exception e){
        e.printStackTrace();

        return R.error().message("执行了全局异常处理");
    }

    /**
     * 特定异常处理  开发中不常用
     * @param e
     * @return
     */
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody//因为不是在controller下  返回数据需要加这个注解
    public  R  error(ArithmeticException e){
        e.printStackTrace();

        return R.error().message("执行了ArithmeticException异常处理");
    }

    /**
     * 自定义异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(GuliException.class)
    @ResponseBody//因为不是在controller下  返回数据需要加这个注解
    public  R  error(GuliException e){
        e.printStackTrace();
        log.error(e.getMessage());
        return R.error().code(e.getCode()).message(e.getMsg());
    }
}
